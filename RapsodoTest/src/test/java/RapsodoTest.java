//SELENIUM
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;




//ASSERTIONS
import junit.framework.Assert;


//JUNIT
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.nio.channels.Selector;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


public class RapsodoTest {

    WebDriver driver;
    WebDriverWait wait;
    JavascriptExecutor js;

    @Before
    public void BeforeTest()
    {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        1- Go to https://rapsodo.com/
        driver.get("https://rapsodo.com/");
        wait = new WebDriverWait(driver, 30);
         js = (JavascriptExecutor) driver;
    }

    @Test
    public void NewsHasAuthor() throws InterruptedException {

//        2- Click "DIAMOND SPORTS" option from "SHOP" menu item. Verify followings;
//        a. url opening as diamond sports category (?category=diamond-sports)
//        b. There is "0 items" at the cart and the price amount is "$0.00" (check it from the upper right corner)
            WebElement shopMenuItem = driver.findElement(By.id("menu-item-dropdown-529"));
            Actions action = new Actions(driver);
            action.moveToElement(shopMenuItem).perform();

            WebElement diamondSportItem = driver.findElement(By.xpath("//li[@id='menu-item-6757']"));
            diamondSportItem.click();

            String DiamondSportsUrl = driver.getCurrentUrl();
            assertTrue(DiamondSportsUrl.contains("?category=diamond-sports"));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"content\"]/header/nav/div/div/div[7]/a")));

            WebElement shopincarditem = driver.findElement(By.xpath("//*[@id=\"content\"]/header/nav/div/div/div[7]/a"));

                    assertTrue(shopincarditem.getText().trim().equals("0 items - $0.00"));


//        3- Click "HITTING 2.0" product. Verify followings;

        WebElement hittingitem = driver. findElement(By.xpath("//*[@id=\"main\"]/div[2]/div/div[4]/a/div[1]/img"));
        hittingitem.click();
        Thread.sleep(2000);
        String HittingUrl = driver.getCurrentUrl();

        //        a. url opening as hitting monitor (/product/rapsodo-hitting-monitor/)
        assertTrue(HittingUrl.contains("/product/rapsodo-hitting-monitor/"));
//        b. the browser title is the same with the product name (Hitting 2.0 - Rapsodo)
        String title = driver.getTitle();
        assertTrue(title.equals("Hitting 2.0 - Rapsodo"));

//        c. product's SKU value is RBB01H.
        WebElement skuItem = driver.findElement(By.xpath("//span[@class='sku_wrapper ']"));
        assertTrue(skuItem.getText().split(":")[1].trim().equals("RBB01H"));

//        d. "ADD TO CART" button is disabled.
        WebElement addcartButton = driver.findElement(By.xpath("//button[@name='add-to-cart']"));
        assertEquals(addcartButton.isEnabled(),true);


//        4- Choose "No Plan" as option. Verify followings;
        Select planSelector = new Select(driver.findElement(By.xpath("//select[@id='stripe_plan_id']")));
planSelector.selectByIndex(1);

//        a. Price is showed as $4,000.00 below the combobox.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='woocommerce-variation-price']/span")));

        WebElement priceControl = driver.findElement(By.xpath("//div[@class='woocommerce-variation-price']/span"));
assertEquals(priceControl.getText(),"$4,000.00");

//            b. "ADD TO CART" button is enabled.
        assertFalse(addcartButton.getAttribute("class").contains("disabled"));

//        5- Click "ADD TO CART" button. Verify followings;
        addcartButton.click();

//        a. url navigates to cart (/cart/)
        Thread.sleep(2000);
        assertTrue(driver.getCurrentUrl().contains("/cart/"));

//        b. There is "1 item" at the cart and the price amount is "$4,000.00" (check it from the upper right
//        corner)
        WebElement basketControl = driver.findElement(By.xpath("//*[@id=\"content\"]/header/nav/div/div/div[7]/a"));
        assertEquals(basketControl.getText(),"1 item - $4,000.00");

//        6- Write a random "Coupon code" and click "APPLY COUPON" button. Verify following;
        String couponCode = "test1234";
        WebElement cuponText = driver.findElement(By.xpath("//input[@name='coupon_code']"));
      wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='close']"))).click();
        cuponText.sendKeys(couponCode);
             //cokie uyarısını geçmek için
        js.executeScript("window.scrollBy(0,500)");
        WebElement appyCoupon = driver.findElement(By.xpath("//*[@id=\"post-368\"]/div/div/form/div[2]/table/tbody/tr[2]/td/div/div/button"));
        appyCoupon.click();

//        a. Check the error message (Coupon "{CouponCode}" does not exist!)
        WebElement alertCoupon = driver.findElement(By.xpath("//ul[@role='alert']/li"));
        assertEquals(alertCoupon.getText(),String.format("Coupon \"%s\" does not exist!",couponCode));
    }

    @After
    public void AfterTest()
    {
        driver.quit();
    }


}
